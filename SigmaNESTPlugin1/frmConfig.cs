﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SigmaNEST;
using Ini;
using System.IO;

namespace SNPlugin
{
    public partial class frmConfig : Form
    {
        ISNApp FSNApp;
        IniFile FConFigIni;
        String Language;
        string GblEncoding = "shift_jis";
        string SettingFile;
        string DBString;
        public frmConfig(ISNApp ASNApp, string AIniFileName, string SetLangauge)
        {
            InitializeComponent();
            Language = SetLangauge;
            FSNApp = ASNApp;
            DBString = FSNApp.ADOConnectionString;
            FConFigIni = new IniFile(AIniFileName);
        }

        private void frmConfig_Load(object sender, EventArgs e)
        {
            ReadINI();
            SetLanguage();
        }
        private void SetLanguage()
        {
            if (Language == "ja") { this.Text = "設定"; } else { this.Text = "Config"; }
        }
        #region "Settings"

        private void ReadINI()
        {
            try
            {
                SettingFile = FConFigIni.IniReadValue("Settings", "SettingFile");
                txtSetting.Text = SettingFile;
                if (FConFigIni.IniReadValue("Settings", "SaveFile") != "") { chkSaveFile.Checked = Boolean.Parse(FConFigIni.IniReadValue("Settings", "SaveFile")); }
            }
            catch (ArgumentNullException e)
            {
                MessageBox.Show(e.Message);
            }
        }
        private void SaveINI()
        {
            try
            {
                FConFigIni.IniWriteValue("Settings", "SettingFile", txtSetting.Text);
                FConFigIni.IniWriteValue("Settings", "SaveFile", chkSaveFile.Checked.ToString());
            }
            catch (ArgumentNullException e)
            {
                MessageBox.Show(e.Message);
            }
        }
        #endregion

        private void btnOpenFileDialog_Click(object sender, EventArgs e)
        {
            txtSetting.Text = SaveFileDialog("CSV files (*.csv)|*.csv|XLS files (*.xls)|*.xls|XLSX files (*.xlsx)|*.xlsx|All files (*.*)|*.*");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveINI();
            Close();
        }
        #region "Dialogs"
        public string OpenFileDialog(string FilterStr)
        {
            string ReturnStr = "";
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = FilterStr;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            ReturnStr = openFileDialog1.FileName;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
            return ReturnStr;
        }
        public string SaveFileDialog(string FilterStr)
        {
            string ReturnStr = "";
            Stream myStream = null;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = FilterStr;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = saveFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            ReturnStr = saveFileDialog1.FileName;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
            return ReturnStr;
        }
        public string FolderBrowserDialog()
        {
            string ReturnStr = "";
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string folderName = folderBrowserDialog1.SelectedPath + "\\";
                ReturnStr = folderName;
            }
            return ReturnStr;
        }
        #endregion

    }
}
