﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SigmaNEST;
using Ini;

namespace SNPlugin
{
    public partial class frmExecute : Form
    {
        ISNApp FSNApp;
        IniFile FConFigIni;
        String Language;
        string GblEncoding = "shift_jis";
        string DBString;
        public frmExecute(ISNApp ASNApp, string AIniFileName, string SetLangauge)
        {
            InitializeComponent();
            Language = SetLangauge;
            FSNApp = ASNApp;
            DBString = FSNApp.ADOConnectionString;
            FConFigIni = new IniFile(AIniFileName);
        }
        private void frmExecute_Load(object sender, EventArgs e)
        {
            ReadINI();
            
        }
        #region "Settings"

        private void ReadINI()
        {
            try
            {
                //CSVPath = FConFigIni.IniReadValue("Settings", "CSVPath");
  
            }
            catch (ArgumentNullException e)
            {
                MessageBox.Show(e.Message);
            }
        }
        private void SaveINI()
        {
            try
            {
                //FConFigIni.IniWriteValue("Settings", "CSVPath", txtCSVPath.Text);
       
            }
            catch (ArgumentNullException e)
            {
                MessageBox.Show(e.Message);
            }
        }
        #endregion
        #region "Navigation"
        private void btnOK_Click(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

    }
}
